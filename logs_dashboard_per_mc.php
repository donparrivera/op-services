<?php
    include "init.php";
    date_default_timezone_set("Asia/Manila");
    //echo date_default_timezone_get();
    //echo "<br/>";
    //echo date("Y-m-d g:i:s");
    //echo "</br/>";
    
    
    #Get timeframe
    $minutes=$_GET['minutes'];
    $current_date=date('Ymd');
    $from_date= date("Y-m-d g:i:s",strtotime("-$minutes minutes"));
    $tools= New Tools;
    $mc1="112.198.100.220";
    $mc2="112.198.100.212";
    $mc3="112.198.100.204";
    $mc4="112.198.100.196";
    $mc5="112.198.100.156";
    $ruckus="112.198.100.186";
    $aruba_codeOK='"code":"0"';
    
    $mc1_params=array("date"=>$current_date,"from_date"=>$from_date,"ip"=>$mc1,"code"=>$aruba_codeOK);
    $log_mc1=$tools->LogsPerMC($mc1_params);
    $mc2_params=array("date"=>$current_date,"from_date"=>$from_date,"ip"=>$mc2,"code"=>$aruba_codeOK);
    $log_mc2=$tools->LogsPerMC($mc2_params);
    $mc3_params=array("date"=>$current_date,"from_date"=>$from_date,"ip"=>$mc3,"code"=>$aruba_codeOK);
    $log_mc3=$tools->LogsPerMC($mc3_params);
    $total_per_mc=array(
                            'mc 1'   =>$log_mc1,
                            'mc 2'   =>$log_mc2,
                        );
    $arr = array(
                    'code'      => '200',
                    'status'    => 'LG002',
                    'data'      => $total_per_mc,
                );
    echo json_encode($arr);
?>