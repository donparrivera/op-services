<?php

#header("Access-Control-Allow-Origin: *");
#header("Content-Type: application/json; charset=UTF-8");
class Tools
{
	private $host="unifidbinstance-staging.cpvsue4umdzy.ap-southeast-1.rds.amazonaws.com";
	private $user="project_douglas";
	private $pass="P23ChtDo01flsr0j";
	private $db="Captive_Log";
	public static function curlPost($url,$content_type,$post_fields)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($content_type));
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT ,60);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post_fields);
		$output = curl_exec($ch);
		return $output;
		curl_close($ch);
	}
	public function connection()
	{
		return $mysqli=mysqli_connect($this->host,$this->user,$this->pass,$this->db);
	}
	public function TotalLogs($data=array())
	{
		extract($data);
		$mysqli=$this->connection();
		$sql="	SELECT * 
							FROM Captive_Log.nas_log_".$date."
							WHERE created_at BETWEEN '".$from_date."' AND NOW()";
		$result=$mysqli->query($sql);
		$total=$result->num_rows;
		return $total;
	}
	public function LogsPerCodes($data=array())
	{
		extract($data);
		$mysqli=mysqli_connect($this->host,$this->user,$this->pass,$this->db);
		$sql="	SELECT * 
				FROM Captive_Log.nas_log_".$date." 
				WHERE (response LIKE '%".$code."%')";
		$result=$mysqli->query($sql);
		$result_total=$result->num_rows;
		if($result_total==0 && $total_logs==0)
		{
			$logs=0;
		}
		else
		{
			$logs=($result_total / $total_logs) * 100;
		}
		
		return $logs;
	}
	public function LogsPerMC($data=array())
	{
		//SELECT * FROM Captive_Log.nas_log_20180402 WHERE created_at BETWEEN '2018-04-01 00:00:00' AND NOW()
		extract($data);
		$mysqli=mysqli_connect($this->host,$this->user,$this->pass,$this->db);
		$sql="	SELECT * 
				FROM Captive_Log.nas_log_".$date." 
				WHERE (url LIKE '%".$ip."%'
				AND (response LIKE '%".$code."%'))
				AND created_at BETWEEN '".$from_date."' AND NOW()";
		$result=$mysqli->query($sql);
		$result_total=$result->num_rows;
		return $result_total;
	}
    
}
/*
$data_string = json_encode($data);
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
												'Content-Type: application/json',
												'Content-Length:'.strlen($data_string))
											);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
	// Send the request
	$result = curl_exec($curl);
	// Free up the resources $curl is using
	return $result;
	curl_close($curl);
*/
?>