<?php
//$database= new SQLiteDatabase('myDatabase.db');
include "init.php";
$tools= New Tools;
date_default_timezone_set("Asia/Manila");
#Get timeframe
$minutes=$_GET['minutes'];
$current_date=date('Ymd');
$from_date= date("Y-m-d g:i:s",strtotime("-$minutes minutes"));
$total_logs=$tools->TotalLogs(array("date"=>$current_date,"from_date"=>$from_date));//$result->num_rows;

$aruba_code0='"code":"0"';
$aruba_code1='"code":"1"';
$aruba_code4='"code":"4"';
$aruba_code_false="false";
$aruba_codeOK='"code":"0k"';
$ruckus_code101='"ResponseCode":101';
$ruckus_code200='"ResponseCode":200';
$ruckus_code201='"ResponseCode":201';
$ruckus_code301='"ResponseCode":301';


//get aruba code 0 logs percentage
$aruba_code0_params=array('date'=>$current_date,'code'=>$aruba_code0,'total_logs'=>$total_logs);
$log_aruba_code0=$tools->LogsPerCodes($aruba_code0_params);
//get aruba code 0 logs percentage

//get aruba code 1 logs percentage
$aruba_code1_params=array('date'=>$current_date,'code'=>$aruba_code1,'total_logs'=>$total_logs);
$log_aruba_code1=$tools->LogsPerCodes($aruba_code1_params);
//get aruba code 1 logs percentage

//get aruba code 4 logs percentage
$aruba_code4_params=array('date'=>$current_date,'code'=>$aruba_code4,'total_logs'=>$total_logs);
$log_aruba_code4=$tools->LogsPerCodes($aruba_code4_params);
//get aruba code 4 logs percentage

//get aruba code false logs percentage
$aruba_code_false_params=array('date'=>$current_date,'code'=>$aruba_code_false,'total_logs'=>$total_logs);
$log_aruba_code_false=$tools->LogsPerCodes($aruba_code_false_params);
//get aruba code false logs percentage

//get aruba code ok logs percentage
$aruba_code_ok_params=array('date'=>$current_date,'code'=>$aruba_codeOK,'total_logs'=>$total_logs);
$log_aruba_code_ok=$tools->LogsPerCodes($aruba_code_ok_params);
//get aruba code ok logs percentage

//get ruckus code 101 logs percentage
$ruckus_code101_params=array('date'=>$current_date,'code'=>$ruckus_code101,'total_logs'=>$total_logs);
$log_ruckus_code101=$tools->LogsPerCodes($ruckus_code101_params);
//get ruckus code 101 logs percentage

//get ruckus code 200 logs percentage
$ruckus_code200_params=array('date'=>$current_date,'code'=>$ruckus_code200,'total_logs'=>$total_logs);
$log_ruckus_code200=$tools->LogsPerCodes($ruckus_code200_params);
//get ruckus code 200 logs percentage

//get ruckus code 201 logs percentage
$ruckus_code201_params=array('date'=>$current_date,'code'=>$ruckus_code201,'total_logs'=>$total_logs);
$log_ruckus_code201=$tools->LogsPerCodes($ruckus_code201_params);
//get ruckus code 201 logs percentage

//get ruckus code 301 logs percentage
$ruckus_code301_params=array('date'=>$current_date,'code'=>$ruckus_code301,'total_logs'=>$total_logs);
$log_ruckus_code301=$tools->LogsPerCodes($ruckus_code301_params);
//get ruckus code 301 logs percentage
$total_codes=array(
                        'code 0'     =>$log_aruba_code0,
                        'code 1'     =>$log_aruba_code1,
                        'code 4'     =>$log_aruba_code4,
                        'code false' =>$log_aruba_code_false,
                        'code OK'    =>$log_aruba_code_ok,
                        'code 101'   =>$log_ruckus_code101,
                        'code 200'   =>$log_ruckus_code200,
                        'code 201'   =>$log_ruckus_code201,
                        'code 301'   =>$log_ruckus_code301,
                        
                   );
/*
$arr = array(
                'code'      => '200',
                'status'    => 'LG001',
                'data'      => $total_codes,
                'total'      =>$total_logs,
            );
*/
$obj= new stdClass();
$obj->code=200;
$obj->status="LG001";
$obj->total=$total_logs;
$obj->data=$total_codes;
echo json_encode($obj,JSON_PRETTY_PRINT);
//echo $total.$failed_total;
//$result=mysqli_result($mysqli,$query);
//echo var_dump($result);
//echo var_dump($result_obj);
//echo json_encode($response,JSON_PRETTY_PRINT);
?>